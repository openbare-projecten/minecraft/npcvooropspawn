package nl.robertvankammen.npcspawnplugin;

import com.destroystokyo.paper.event.player.PlayerUseUnknownEntityEvent;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import com.mojang.datafixers.util.Pair;
import net.kyori.adventure.text.Component;
import net.minecraft.network.protocol.game.*;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.item.Items;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_19_R2.CraftServer;
import org.bukkit.craftbukkit.v1_19_R2.CraftWorld;
import org.bukkit.craftbukkit.v1_19_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.List;

import static java.util.UUID.randomUUID;
import static net.minecraft.world.entity.EquipmentSlot.MAINHAND;

public final class NpcForSpawnPlugin extends JavaPlugin implements Listener {

  private static final String TEXTURES = "textures";

  private static final double X = -57.5;
  private static final double Y = 71.0;
  private static final double Z = -6.5;

  //https://mineskin.org/gallery
  private static final String SKIN_VALUE = "ewogICJ0aW1lc3RhbXAiIDogMTYzOTMyOTczMDE2MCwKICAicHJvZmlsZUlkIiA6ICI2NmI0ZDRlMTFlNmE0YjhjYTFkN2Q5YzliZTBhNjQ5OSIsCiAgInByb2ZpbGVOYW1lIiA6ICJBcmFzdG9vWXNmIiwKICAic2lnbmF0dXJlUmVxdWlyZWQiIDogdHJ1ZSwKICAidGV4dHVyZXMiIDogewogICAgIlNLSU4iIDogewogICAgICAidXJsIiA6ICJodHRwOi8vdGV4dHVyZXMubWluZWNyYWZ0Lm5ldC90ZXh0dXJlL2FiNTFhNTkxYTAwZDM2MjY1ZWNmYzMzM2RiNGNjMDRmMWYxZDY0OWJjM2Y0NGRkODYzZTVmNzYxYjExNmUwMzEiCiAgICB9CiAgfQp9";
  private static final String SKIN_SIGNATURE = "N7JvqqjMiI9e1iXFe1gGf3NY3ON1RJnWDiBToXM1kZ4ULCqFn+ECwHeuFgFI56XrIJ0K/lfLKsi9T4HylI83d52iDRB2ADX9521OIrfVlDxeNj+zmfTNFsUlLDab9J6j1gW3M/Yg12HRjVi2w0P3CddLvLctvnzMNh3Fj7gXWy2dQWWPECKyq1W37QjwuneF4ZKfr+7Dea5mURC4hSuZooGfa2uQ00dRb7do3c0ylijrU/vCifqd5r+bceIvTi4g6x+2iDLIZDXuUxmATOFMHoa1zWtWEspQUCKq4DqjvBFKnILvV1MDKZRWH+b/kz6J834h0itJp1JCgcQiejzFSzj9Y1K4wIUlBCTObcD0cKqT97oWdGz4eOu0lSIzF0YlQap5ZHu2Hz+1mAKZHOv7OL0yNhQMXEZuXLPLmMcRbTXdHFwHTcdC5XH+EZ7zXpqoq9eDbr6JGaGl9QNsMZ6tIDilzbSKYlUndVnfTJRF9HriyWSFWJ119T33o5rVMpnnR8UW8a30CYfVbZMDLDi4qEzDyEVQ+TLDO96ldusn/Co/UR+7gWde7k7LUIm0Hds0LCB2RR1jGXbcfyMbnvPpkHsJb85e2e24ixA9Ro7kqRf04Gy6vCxWuKOBB/GGcdOQn+GmosTM2C9tUcSrR1rog6QjK/AadNsleQkS4Ww0OW0=";
  private ServerPlayer npc;

  @Override
  public void onEnable() {
    this.getServer().getPluginManager().registerEvents(this, this);
    Bukkit.getScheduler().scheduleSyncDelayedTask(this, this::createNPC, 100); // hack zodat de wereld geladen is
    Bukkit.getScheduler().scheduleSyncRepeatingTask(this, this::updateNpcRotation, 1, 1);
  }

  private void updateNpcRotation() {
    if (npc == null)
      return;
    for (Player p : Bukkit.getOnlinePlayers()) {
      if (!p.getWorld().getName().equals("world")) continue;
      if (calculateDistance(p) > 5) continue;

      var connection = ((CraftPlayer) p).getHandle().connection;
      var difference = p.getLocation().subtract(npc.getBukkitEntity().getLocation()).toVector().normalize();
      var degrees = (float) Math.toDegrees(Math.atan2(difference.getZ(), difference.getX()) - Math.PI / 2);
      var angle = (byte) ((degrees * 256.0F) / 360.0F);

      var height = npc.getBukkitEntity().getLocation().subtract(p.getLocation()).toVector().normalize();
      var pitch = (byte) ((Math.toDegrees(Math.atan(height.getY())) * 256.0F) / 360.0F);

      connection.send(new ClientboundRotateHeadPacket(npc, angle));
      connection.send(new ClientboundMoveEntityPacket.Rot(npc.getId(), angle, pitch, true));
    }
  }

  @EventHandler
  public void playerInteractEntityEvent(PlayerUseUnknownEntityEvent playerInteractEntityEvent) {
    var player = playerInteractEntityEvent.getPlayer();
    var location = player.getLocation();
    var x = location.getX();
    var y = location.getY();
    var z = location.getZ();
    var worldName = location.getWorld().getName();
    if (worldName.equals("world") && checkSpawnLocation(x, y, z)) {
      openBook(player);
    }
  }

  private boolean checkSpawnLocation(double x, double y, double z) {
    return x >= -65 && x <= -50
      && y >= 60 && y <= 80
      && z >= -15 && z <= 0;
  }

  @EventHandler
  public void onJoin(PlayerJoinEvent joinEvent) {
    if (npc != null && joinEvent.getPlayer().getWorld().getName().equals("world")) {
      addNPCPacket(npc, (CraftPlayer) joinEvent.getPlayer());
    }
  }

  @EventHandler
  public void onNetherjoin(PlayerChangedWorldEvent playerChangedWorldEvent) {
    if (npc != null && playerChangedWorldEvent.getPlayer().getWorld().getName().equals("world")) {
      addNPCPacket(npc, (CraftPlayer) playerChangedWorldEvent.getPlayer());
    }
  }

  private void openBook(Player player) {
    var book = new ItemStack(Material.WRITTEN_BOOK); //Create book ItemStack
    book.editMeta(BookMeta.class, meta -> {
      meta.setTitle("Regels");
      meta.setAuthor("Server");
      meta.addPages(Component.text("""
          Hallo gezellige mensen!!

          Zoals jullie zien spawn je op een hele mooie bijzondere plek. Dit is de shopping district. We gaan de shops bouwen in en rond de bergen!! wees creatief!!

          verder hebben we een aantal regels zoals"""),
        Component.text("""
          normaal:

          1. Ga respectvol om met de gebouwen en bases van andere mensen. Elkaar pranken mag maar je moet het ook weer opruimen en eventueel maken als dat nodig is.

          2. Cheats zijn niet toegestaan, zoals"""),
        Component.text("""
          xray , fly en duplication glitches etc.

          3. Hou de chat een beetje gezellig, niet vloeken en schelden

          4. Als je bij iemand in de buurt wil bouwen moet je eerst even toestemming vragen aan diegene, het kan\s"""),
        Component.text("""
          zo zijn dat die persoon daar al iets in gedachten heeft. Wie ergens als eerste zijn base heeft geclaimd heeft daar als eerste recht op.
          iets claimen op de dinmap kan je aangeven bij Palmboom1212.

          5. Je base mag je
          """),
        Component.text("""
          maken vanaf 750 blokken om spawn heen.

          6. Neem je goede humeur mee

          Nog een nieuwe toevoeging aan deze server is dat we pas later de end in willen dan normaal. Dit betekend dat de end"""),
        Component.text("""
          zowizo 1 maand dicht blijft. Na deze maand gaan we kijken hoe ver iedereen is en of het dan de juiste tijd is om de draak te verslaan.

          Ook hebben we een mod geïnstallerd waarbij je je skills kunt levellen. Dit gaat automatisch. Wat je hieraan hebt kun je"""),
        Component.text("""
          vinden op de volgende website: https://mcmmo.org/wiki/Main_Page


          Hebben jullie vragen dan kunnen jullie die stellen op discord

          VEEL PLEZIER!!!""")); //Add a page
    });
    var inventory = player.getInventory();
    var oldItem = inventory.getItemInMainHand(); //Get item in hand so we can set it back
    inventory.setItemInMainHand(book); //Set item in hand to book
    player.openBook(book);
    inventory.setItemInMainHand(oldItem); //Set item in hand back
  }

  private double calculateDistance(Player p) {
    var diffX = this.npc.getX() - p.getLocation().getX();
    var diffZ = this.npc.getZ() - p.getLocation().getZ();
    var x = diffX < 0 ? (diffX * -1) : diffX;
    var z = diffZ < 0 ? (diffZ * -1) : diffZ;
    return Math.sqrt(Math.pow(x, 2) + Math.pow(z, 2));
  }

  public void createNPC() {
    var server = ((CraftServer) Bukkit.getServer()).getServer();
    var world = ((CraftWorld) Bukkit.getWorld("world")).getHandle(); // Change "world" to the world the NPC should be spawned in.
    var gameProfile = getProfiel();
    npc = new ServerPlayer(server, world, gameProfile);
    npc.setPos(X, Y, Z);
    setSkinn(gameProfile);
    addNPCPacket(npc);
  }

  public GameProfile getProfiel() {
    return new GameProfile(randomUUID(), "Ivar");
  }

  private void setSkinn(GameProfile gameProfile) {
    gameProfile.getProperties().get(TEXTURES).clear();
    gameProfile.getProperties().put(TEXTURES, new Property(TEXTURES, SKIN_VALUE, SKIN_SIGNATURE));
  }

  public void addNPCPacket(ServerPlayer npc) {
    Bukkit.getOnlinePlayers().forEach(player -> {
      addNPCPacket(npc, (CraftPlayer) player);
    });
  }

  public void addNPCPacket(ServerPlayer npc, CraftPlayer player) {
    var connection = player.getHandle().connection;
    npc.getEntityData().set(net.minecraft.world.entity.player.Player.DATA_PLAYER_MODE_CUSTOMISATION, (byte) 126);
    connection.send(new ClientboundPlayerInfoUpdatePacket(ClientboundPlayerInfoUpdatePacket.Action.ADD_PLAYER, npc));
    connection.send(new ClientboundAddPlayerPacket(npc));
    connection.send(new ClientboundSetEntityDataPacket(npc.getId(), npc.getEntityData().packDirty()));
    net.minecraft.world.item.ItemStack itemstack = new net.minecraft.world.item.ItemStack(Items.IRON_AXE);
    var equipmentList = List.of(Pair.of(MAINHAND, itemstack));
    connection.send(new ClientboundSetEquipmentPacket(npc.getId(), equipmentList));

//    Bukkit.getScheduler().scheduleSyncDelayedTask(this, () -> {
//      connection.send(new ClientboundPlayerInfoUpdatePacket(ClientboundPlayerInfoUpdatePacket.Action.re, npc));
//    }, 100);
  }
}
